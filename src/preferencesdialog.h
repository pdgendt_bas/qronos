#pragma once

#include <QCheckBox>
#include <QDialog>
#include <QLineEdit>
#include <QLabel>

#include "jira/jiraapi.h"

namespace net::tibault::qronos {

class PreferencesDialog : public QDialog
{
    Q_OBJECT
public:
    PreferencesDialog(std::shared_ptr<jira::JiraApi> api,
                      QWidget* parent = nullptr);
private:
    void updateStatus(const jira::promise<jira::User>& info);
    void save();

    std::shared_ptr<jira::JiraApi> _api;

    QLineEdit* _domain;
    QLineEdit* _user;
    QLineEdit* _token;

    QLabel* _status;

    QCheckBox* _pauseOnLock;
};

}
