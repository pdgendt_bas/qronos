#pragma once

#include <QWidget>

class QMenu;

namespace net::tibault::qronos {

class StatusBanner : public QWidget
{
    Q_OBJECT
public:
    explicit StatusBanner(QWidget *parent = nullptr);

    void setText(QString text);
    void clear();

    void setMenu(QMenu* menu);

    virtual QSize sizeHint() const override;
    virtual QSize minimumSizeHint() const override;

protected:
    virtual void paintEvent(QPaintEvent *event) override;
    virtual void showEvent(QShowEvent *event) override;

private:
    void positionBanner();
    QString text(int elideMax = 0) const;

    QString _text;
    QMenu* _menu;
};

}
