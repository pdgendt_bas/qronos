#include "svg.h"

#include <QSvgRenderer>
#include <QSystemTrayIcon>
#include <QPainter>

namespace net::tibault::util {

QPixmap svgToPixmap(const QString &filePath, const QSize &size)
{
    QImage img{size, QImage::Format_ARGB32_Premultiplied};
    img.fill(Qt::transparent);

    QSvgRenderer svg{filePath};
    QPainter svgPainter{&img};
    svg.render(&svgPainter);

    return QPixmap::fromImage(img);
}

QPixmap svgToPixmap(const QByteArray &contents, const QSize &size)
{
    QImage img{size, QImage::Format_ARGB32_Premultiplied};
    img.fill(Qt::transparent);

    QSvgRenderer svg{contents};
    QPainter svgPainter{&img};
    svg.render(&svgPainter);

    return QPixmap::fromImage(img);
}

}
