#include "statusbanner.h"

#include <QGuiApplication>
#include <QMenu>
#include <QPainter>
#include <QPaintEvent>
#include <QPainterPath>
#include <QScreen>
#include <QShowEvent>
#include <QSizePolicy>

namespace net::tibault::qronos {

constexpr int TextMaxLen = 420;
constexpr int TextMarginH = 5;
constexpr int TextMarginV = 2;

StatusBanner::StatusBanner(QWidget *parent)
    : QWidget(parent)
{
    setWindowFlags(Qt::FramelessWindowHint
                    | Qt::WindowStaysOnTopHint
                    | Qt::Tool
                    | Qt::WindowDoesNotAcceptFocus);
    setAttribute(Qt::WA_TranslucentBackground);
    setAttribute(Qt::WA_MacAlwaysShowToolWindow);
    setSizePolicy(QSizePolicy::Fixed,
                  QSizePolicy::Fixed);

    auto f = font();
    f.setPointSizeF(10.0);
    setFont(f);

    auto pal = palette();
    pal.setColor(QPalette::Window,
                 QColor(33, 34, 34));
    pal.setColor(QPalette::WindowText,
                 QColor(190, 192, 193));
    setPalette(pal);

    connect(this, &StatusBanner::customContextMenuRequested,
            this, [this](const QPoint& pos){
        if (_menu) {
            _menu->popup(mapToGlobal(pos));
        }
    });

    clear();
}

void StatusBanner::setText(QString txt)
{
    _text = txt.simplified();
    if (_text.isEmpty()) {
        _text = "...";
    }

    setToolTip(text());

    updateGeometry();
    adjustSize();
    positionBanner();
    update();
}

void StatusBanner::clear()
{
    setText({});
}

void StatusBanner::setMenu(QMenu *menu)
{
    _menu = menu;
    setContextMenuPolicy(menu ? Qt::CustomContextMenu
                              : Qt::NoContextMenu);
}

QSize StatusBanner::sizeHint() const
{
    auto t = text(TextMaxLen);
    auto s = fontMetrics().size(0, t);
    return s + QSize(2 * TextMarginH,
                     2 * TextMarginV);
}

QSize StatusBanner::minimumSizeHint() const
{
    return sizeHint();
}

void StatusBanner::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);
    p.setOpacity(0.9);

    auto r = event->rect();

    QPainterPath pp;
    pp.addRoundedRect(r, 2.5, 2.5);
    p.fillPath(pp, palette().color(QPalette::Window));

    p.drawText(r.adjusted(TextMarginH, TextMarginV,
                          -TextMarginH, -TextMarginV),
               text(TextMaxLen));
}

void StatusBanner::showEvent(QShowEvent* event)
{
    positionBanner();

    QWidget::showEvent(event);
}

void StatusBanner::positionBanner()
{
    auto screenRect = QGuiApplication::primaryScreen()->availableGeometry();
    QRect ourRect{pos(), sizeHint()};

    //move ourRect to top center of screenRect
    ourRect.moveCenter(screenRect.center());
    ourRect.moveTop(screenRect.top() + 3);

    move(ourRect.topLeft());
}

QString StatusBanner::text(int elideMax) const
{
    if (elideMax > 0) {
        auto fm = fontMetrics();
        auto txt = fm.elidedText(_text,
                                 Qt::ElideRight,
                                 elideMax);
    }
    return _text;
}

}
