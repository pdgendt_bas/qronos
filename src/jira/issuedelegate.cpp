#include "issuedelegate.h"

#include <QPainter>

namespace net::tibault::jira {

static const qreal IssueDelegateMargin{5.0};
static const qreal IssueDelegateIconSize{32.0};

IssueDelegate::IssueDelegate(QObject *parent)
    : QStyledItemDelegate(parent)
{
    // empty
}

void IssueDelegate::paint(QPainter *painter,
                          const QStyleOptionViewItem &option,
                          const QModelIndex &index) const
{
    painter->save();
    painter->setRenderHint(QPainter::Antialiasing);

    // Selection setup

    auto pen = painter->pen();
    if (option.state & QStyle::State_Selected) {
        painter->fillRect(option.rect,
                          option.palette
                                .brush(QPalette::Highlight));
        pen.setBrush(option.palette.brush(QPalette::HighlightedText));
    } else {
        pen.setBrush(option.palette.brush(QPalette::Text));
    }
    painter->setPen(pen);

    // Icon

    auto ico = index.data(Qt::DecorationRole).value<QIcon>();
    if (!ico.isNull()) {
        QRectF icoRect = option.rect;
        icoRect.setSize({IssueDelegateIconSize,
                         IssueDelegateIconSize});
        icoRect.adjust(IssueDelegateMargin, IssueDelegateMargin,
                       IssueDelegateMargin, IssueDelegateMargin);

        auto img = ico.pixmap({static_cast<int>(IssueDelegateIconSize),
                               static_cast<int>(IssueDelegateIconSize)},
                              QIcon::Normal,
                              QIcon::On)
                      .toImage();
        painter->drawImage(icoRect, img);
    }

    // Issue title

    QRectF tRect = option.rect;
    tRect.adjust(IssueDelegateMargin + IssueDelegateIconSize + IssueDelegateMargin,
                 IssueDelegateMargin,
                 -IssueDelegateMargin,
                 -IssueDelegateIconSize/2 - IssueDelegateMargin);

    auto fm = option.fontMetrics;
    auto text = fm.elidedText(index.data(Qt::DisplayRole).toString(),
                              option.textElideMode,
                              static_cast<int>(tRect.width()));
    painter->drawText(tRect, text);

    // Issue key

    tRect.adjust(0.0, IssueDelegateIconSize/2,
                 0.0, IssueDelegateIconSize/2);
    auto smFont = option.font;
    smFont.setItalic(true);
    painter->setFont(smFont);
    fm = painter->fontMetrics();
    painter->drawText(tRect,
                      index.data(Qt::EditRole).toString());

    // End

    painter->restore();
}

QSize IssueDelegate::sizeHint(const QStyleOptionViewItem &option,
                              const QModelIndex &index) const
{
    QSizeF s {
        IssueDelegateMargin
            + IssueDelegateIconSize
            + IssueDelegateMargin
            + option.fontMetrics
                    .horizontalAdvance(index.data(Qt::DisplayRole)
                                            .toString())
            + IssueDelegateMargin,
        IssueDelegateMargin
            + IssueDelegateIconSize
            + IssueDelegateMargin
    };

    return s.toSize();
}

}
